import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailSpinner extends StatefulWidget {
  final String value1;

  DetailSpinner({super.key, required this.value1});

  @override
  State<DetailSpinner> createState() => _DetailSpinnerState();
}

class _DetailSpinnerState extends State<DetailSpinner> {
  final List<String> listPay = ['Đóng phí 1', 'Đóng phí 2', 'Đóng phí 3'];
  final List<String> listRegistry = ['Kênh 1', 'Kênh 2', 'Kênh 3'];
  final List<String> listState = ['State 1', 'State 2', 'State3'];
  final List<String> listStatus = ['Status 1', 'Status 2', 'Status 3'];
  final List<String> list = [];

  // late String result = 'Tất cả';
  Map<String, String> result={};

  @override
  Widget build(BuildContext context) {
    if (widget.value1 == 'idPay') {
      listPay.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idRegistry') {
      listRegistry.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idState') {
      listState.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idStatus') {
      listStatus.map((e) => list.add(e)).toList();
    }
    print(widget.value1);
    return Scaffold(
      appBar: AppBar(
        title: Text('Lựa chọn danh mục'),
        backgroundColor: Color(0xFF6F9BD4),
      ),
      body: Align(
        alignment: Alignment.center,
        child: Wrap(
          children: [
            Container(
                width: 300,
                decoration: BoxDecoration(
                  color: Color(0xFF6F9BD4),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Column(
                  children: [
                    ListView.separated(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: list.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          children: [
                            Radio(
                                value: list[index],
                                groupValue: list,
                                onChanged: (value) {
                                  setState(() {
                                    list.clear();
                                    if(value != null){
                                      result[widget.value1] = value.toString();
                                    }                               
                                    print(result);
                                    Navigator.pop(context, result);
                                  });
                                }),
                            SizedBox(
                              width: 30,
                            ),
                            Text(list[index]),
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider();
                      },
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
