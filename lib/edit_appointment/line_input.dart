// import 'dart:js_interop';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'dialogTime.dart';
import 'directory.dart';
import 'edit_schedule.dart';

class LineInput extends StatefulWidget {
  const LineInput(
      {required this.title,
      required this.type,
      required this.index,
      super.key});
  final String title;
  final String type;
  final int index;

  @override
  State<LineInput> createState() => _LineInputState();
}

class _LineInputState extends State<LineInput> {
  DateTime _dateTime = DateTime.now();
  Map<int, String> defaultValues = {
    1: 'Qua VNCare',
    2: 'Viện phí',
    3: 'Sinh Viên',
    4: 'Nam',
    5: 'Kinh',
    6: 'Việt Nam',
    7: 'Thái Bình',
    8: 'Thái Bình',
    9: 'Quỳnh Phụ',
    10: 'Quỳnh Trang',
    11: 'Khám sản',
    12: 'Phòng khám 1',
    13: 'BS Minh Phương',
    15: '09-07-2009',
    21: '09-08-2023 09:00'
  };
  final _textEditingController = TextEditingController();
  void showDataAlert(context, int indexns) {
    showDialog(
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: ShowDialog(indexns: indexns)),
          );
        }).then((value) {
      setState(() {
        defaultValues[widget.index] = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    void transfer() async {
      final result = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  Directorys(title: widget.title, index: widget.index)));
      setState(() {
        defaultValues[widget.index] = result;
      });
    }

    if (widget.type == "input") {
      return Row(
        children: [
          Container(
              margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
              alignment: Alignment.centerLeft,
              width: 150,
              height: 25,
              child: Text(widget.title)),
          Container(
              alignment: Alignment.centerLeft,
              width: 165,
              height: 25,
              child: TextFormField(
                controller: _textEditingController,
                decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.5, color: Colors.black)),
                    fillColor: Colors.blue,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide:
                            BorderSide(color: Colors.grey, width: 0.5))),
              )),
        ],
      );
    } else if (widget.type == 'dropbutton') {
      return Container(
        child: Row(
          children: [
            Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
                alignment: Alignment.centerLeft,
                width: 150,
                height: 25,
                child: Text(widget.title)),
            Container(
              alignment: Alignment.centerLeft,
              width: 175,
              height: 25,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.grey, width: 0.5)),
              //onclick vao
              child: GestureDetector(
                onTap: () {
                  if (widget.index == 21) {
                    showDataAlert(context, widget.index);
                  } else if (widget.index == 15) {
                    showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1920),
                            lastDate: DateTime(2024))
                        .then((value) {
                      setState(() {
                        _dateTime = value!;
                        defaultValues[widget.index] =
                          _dateTime.toString().substring(0, 10);
                      });
                    });
                  } else {
                    transfer();
                  }
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('   ${defaultValues[widget.index].toString()}'),
                      Container(
                        height: 21,
                        margin: const EdgeInsets.only(right: 5),
                        child: widget.title == 'Ngày sinh (*)' ||
                                widget.title == 'Thời gian khám (*)'
                            ? Icon(Icons.calendar_month_outlined)
                            : const Icon(Icons.arrow_drop_down),
                      )
                      // const Icon(Icons.arrow_drop_down)
                    ]),
              ),
            ),
          ],
        ),
      );
    } else {
      return const Text('Ngoại lệ');
    }
  }


}

