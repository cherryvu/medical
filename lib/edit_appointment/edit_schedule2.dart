import 'dart:io';

import 'package:flutter/material.dart';
// import 'package:week2/line_input.dart';

import 'dialogTime.dart';
import 'directory.dart';
import 'line_input.dart';

class EditSchedule2 extends StatefulWidget {
  const EditSchedule2({super.key});
  // final List<String> listItem;
  @override
  State<EditSchedule2> createState() => _EditSchedule2State();
}

class _EditSchedule2State extends State<EditSchedule2> {
  void handleCallback(String value) {
    // Xử lý dữ liệu từ widget con tại đây
    print(value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFEEEEEE),
      appBar: AppBar(
        backgroundColor: Color(0xFF6F9BD4),
        title: const Text("Sửa lịch hẹn khám tại CSYT"),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
                flex: 8,
                child: Container(
                  // height: 120,
                  padding: EdgeInsets.all(10),
                  child: Card(
                    child: Column(
                      children: [
                        const LineInput(
                          title: "Yêu cầu khám (*)",
                          type: "dropbutton",
                          index: 11,
                        ),
                        const LineInput(
                          title: "Phòng khám (*)",
                          type: "dropbutton",
                          index: 12,
                        ),
                        const LineInput(
                          title: "Bác sĩ",
                          type: "dropbutton",
                          index: 13,
                        ),
                        const LineInput(
                          title: "Thời gian khám (*)",
                          type: "dropbutton",
                          index: 21,
                        ),
                        //content
                        Row(
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
                                alignment: Alignment.topLeft,
                                width: 150,
                                height: 25,
                                child: Text('Nội dung khám')),
                            Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(bottom: 10),
                                width: 165,
                                height: 35,
                                child: TextFormField(
                                  textAlign: TextAlign.left,
                                  decoration: const InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 0.5, color: Colors.black)),
                                      fillColor: Colors.blue,
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5)),
                                          borderSide: BorderSide(
                                              color: Colors.grey, width: 1))),
                                )),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
                                alignment: Alignment.topLeft,
                                width: 150,
                                height: 25,
                                child: Text('Ghi chú')),
                            Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(bottom: 10),
                                width: 165,
                                height: 70,
                                child: TextFormField(
                                  // style: TextStyle(fontSize: 12),
                                  decoration: const InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 0.5, color: Colors.black)),
                                      fillColor: Colors.blue,
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5)),
                                          borderSide: BorderSide(
                                              color: Colors.grey, width: 1))),
                                )),
                          ],
                        ),
                        //end content
                      ],
                    ),
                  ),
                )),
            Expanded(
                flex: 1,
                child: Container(
                  height: 30,
                  width: MediaQuery.of(context).size.width,
                  // padding: const EdgeInsets.fromLTRB(15, 5, 15, 15),
                  margin: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: Card(
                    color: Color(0xFF6F9BD4),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.arrow_circle_right_outlined,
                          color: Colors.white,
                        ),
                        Text(
                          ' Tiếp tục',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
