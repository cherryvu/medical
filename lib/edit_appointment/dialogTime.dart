import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:product_layout_app/edit_appointment/alert_dialog.dart';
import 'package:table_calendar/table_calendar.dart';
// import 'package:week2/itemHourTest.dart';

import '../Container/DenyPopup.dart';
// import 'dialogAlert.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'itemHourTest.dart';

class ShowDialog extends StatefulWidget {
  const ShowDialog({required this.indexns, super.key});
  final int indexns;
  @override
  State<ShowDialog> createState() => _ShowDialogState();
}

class _ShowDialogState extends State<ShowDialog> {
  DateTime? _selectedDay;
  DateTime _focusDay = DateTime.now();
  String? timePicked = '';

  Color _colorBackGround = Colors.white;
  Color _colorText = Colors.blue;

  static List<String> chotrong = [
    '08:00 AM',
    '08:30 AM',
    '09:00 AM',
    '09:30 AM',
    '10:00 AM',
    '10:30 AM',
    '14:00 AM',
    '14:30 AM',
    '15:00 AM',
    '15:30 AM',
    '16:00 AM',
  ];
  Map<String, bool> colorPicks = {
    '08:00 AM': false,
    '08:30 AM': false,
    '09:00 AM': false,
    '09:30 AM': false,
    '10:00 AM': false,
    '10:30 AM': false,
    '14:00 AM': false,
    '14:30 AM': false,
    '15:00 AM': false,
    '15:30 AM': false,
    '16:00 AM': false,
  };
  List<int> statePicker = List.filled(chotrong.length, 0);

  void checkTimePicker() {
    if (timePicked.toString().trim() == '') {
      showDialog(
          context: context,
          builder: (context) {
            return AlertPopup();
          });
    } else {
      String backValues =
          _focusDay.toString().substring(0, 10) + " " + timePicked.toString();
      Navigator.pop(context, backValues);
    }
  }

  var _calendarFormat = CalendarFormat.month;
  void onDataReceived(String data) {
    setState(() {
      timePicked = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
            child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                //icon cancel
                Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 20, 0),
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.cancel_rounded)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
                  //tablecalendar
                  child: TableCalendar(
                    locale: "en_US",
                    rowHeight: 33,
                    headerStyle: const HeaderStyle(
                        formatButtonVisible: false, titleCentered: true),
                    availableGestures: AvailableGestures.all,
                    focusedDay: _focusDay,
                    firstDay: DateTime.utc(2000, 10, 16),
                    lastDay: DateTime.utc(2024, 3, 14),
                    selectedDayPredicate: (day) {
                      return isSameDay(_selectedDay, day);
                    },
                    onDaySelected: (selectedDay, focusDay) {
                      setState(() {
                        _selectedDay = selectedDay;
                        _focusDay = focusDay;
                      });
                    },
                    calendarFormat: _calendarFormat,
                    onFormatChanged: (format) {
                      setState(() {
                        _calendarFormat = format;
                      });
                    },
                    onPageChanged: (focusedDay) {
                      _focusDay = focusedDay;
                    },
                  ),
                ),
                //time
                Container(
                  padding: const EdgeInsets.fromLTRB(35, 5, 35, 0),
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      side:
                          const BorderSide(color: Color(0xFF6F9BD4), width: 1),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            height: 30,
                            decoration: const BoxDecoration(
                                color: Color(0xFF6F9BD4),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15))),
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            child: const Text(
                              'Số chỗ còn trống',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.white),
                            ),
                          ),
                          ItemGridViewTest(
                            onItemClick: (String value) {
                              timePicked = value;
                            },
                            colorPick: colorPicks,
                            // onDataReceived: onDataReceived,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //tt
                Container(
                    height: 85,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    padding: const EdgeInsets.all(20),
                    width: MediaQuery.of(context).size.width,
                    child: GestureDetector(
                      onTap: () {
                        checkTimePicker();
                      },
                      child: Card(
                        color: const Color(0xFF6F9BD4),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.arrow_circle_right_outlined,
                              color: Colors.white,
                            ),
                            Center(
                                child: Text(
                              ' Tiếp tục',
                              style: TextStyle(color: Colors.white),
                            )),
                          ],
                        ),
                      ),
                    ))
              ],
            ),
          ),
        )),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

// void showDataAlert(context, String t) {
//   showDialog(
//       context: context,
//       builder: (context) {
//         return Container(
//           child: Container(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(20),
//               ),
//               child: MyWidget(
//                 title: t,
//               )),
//         );
//       });
// }
