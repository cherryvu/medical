import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:product_layout_app/model/lich.dart';


class groupDate extends StatefulWidget {
  const groupDate({super.key});

  @override
  State<groupDate> createState() => _groupDateState();
}

class _groupDateState extends State<groupDate> {
  //notification controller
  final notifiController = TextEditingController();

  // scroll controller
  ScrollController _scrollController = new ScrollController();

  static List<lich> main_hen = [
    // lich(
    //      'Lịch hẹn khám',
    //     '10 phút trước',
    //     DateTime.now().microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
    lich(
      '1',
        'Lịch hẹn khám',
        '15 phút trước',
        DateTime.now().microsecondsSinceEpoch,
        'Nguyễn Thị Ánh Nguyệt(viện phí)',
        'Ngoại tổng hợp',
        'Ngoại tổng hợp',
        '07/07/2021 9:00'),
    lich(
      '1',
        'Lịch hẹn khám',
        '20 phút trước',
        DateTime.now().microsecondsSinceEpoch,
        'Nguyễn Thị Ánh Nguyệt(viện phí)',
        'Ngoại tổng hợp',
        'Ngoại tổng hợp',
        '07/07/2021 9:00'),
    // lich(
    //      'Lịch hẹn khám',
    //     '10:11:20',
    //     DateTime(DateTime.now().year, DateTime.now().month,
    //             DateTime.now().day - 1, 02, 02)
    //         .microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
    // lich(
    //      'Lịch hẹn khám',
    //     '10:11:20',
    //     DateTime(2023, 07, 12, 11, 10).microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
    lich(
      '1',
        'Lịch hẹn khám',
        '10:11:20',
        DateTime(2023, 07, 11, 11, 10).microsecondsSinceEpoch,
        'Nguyễn Thị Ánh Nguyệt(viện phí)',
        'Ngoại tổng hợp',
        'Ngoại tổng hợp',
        '07/07/2021 9:00'),
  ];

  List<lich> display = List.from(main_hen);

  // function to convert time stamp to date
  static DateTime returnDateAndTimeFormat(String time) {
    var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
    var originalDate = DateFormat('MM/dd/yyyy').format(dt);
    return DateTime(dt.year, dt.month, dt.day);
  }

  //function to return message time in 24 hours format AM/PM
  // static String messageTime(String time) {
  //   var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
  //   String difference = '';
  //   difference = DateFormat('jm').format(dt).toString();
  //   return difference;
  // }

  // function to return date if date changes based on your local date and time
  static String groupMessageDateAndTime(String time) {
    var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
    var originalDate = DateFormat('MM/dd/yyyy').format(dt);

    final todayDate = DateTime.now();

    final today = DateTime(todayDate.year, todayDate.month, todayDate.day);
    final yesterday =
        DateTime(todayDate.year, todayDate.month, todayDate.day - 1);
    String difference = '';
    final aDate = DateTime(dt.year, dt.month, dt.day);

    if (aDate == today) {
      difference = " ";
    } else if (aDate == yesterday) {
      difference = "Hôm qua";
    } else {
      difference = DateFormat('MM/dd/yyyy').format(dt).toString();
    }

    return difference;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      // body: SafeArea(
      body:
          // child: Column(
          //   //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //   //crossAxisAlignment: CrossAxisAlignment.stretch,
          //   children: [
          //     Expanded(
          // child: Container(
          //padding: const EdgeInsets.symmetric(vertical: 5),
          //color: Colors.transparent,
          ListView.separated(
              padding: EdgeInsets.only(left: 15, right: 15),
              separatorBuilder: (context, index) => const SizedBox(height: 0),
              controller: _scrollController,

              // reverse: true,
              // shrinkWrap: true,
              //physics: const ClampingScrollPhysics(), // ← can't
              itemCount: main_hen.length,
              itemBuilder: (context, index) {
                bool isSameDate = false;
                String? newDate = '';

                final DateTime date = returnDateAndTimeFormat(main_hen[index].day.toString());

                if (index == 0 && main_hen.length == 1) {
                  newDate =
                      groupMessageDateAndTime(main_hen[index].day.toString())
                          .toString();
                } else if (index == main_hen.length - 1) {
                  newDate =
                      groupMessageDateAndTime(main_hen[index].day.toString())
                          .toString();
                } else {
                  final DateTime date =
                      returnDateAndTimeFormat(main_hen[index].day.toString());
                  final DateTime prevDate = returnDateAndTimeFormat(
                      main_hen[index + 1].day.toString());
                  isSameDate = date.isAtSameMomentAs(prevDate);

                  if (kDebugMode) {
                    print("$date $prevDate $isSameDate");
                  }
                  newDate = isSameDate
                      ? ''
                      : groupMessageDateAndTime(
                              main_hen[index - 1].day.toString())
                          .toString();
                }

                return Container(
                  child: Column(
                    children: [
                      if (newDate.isNotEmpty)
                        Align(
                          alignment: Alignment.topLeft,
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    //borderRadius: BorderRadius.circular(20)
                                    ),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
                                  child: Text(
                                    newDate,
                                    style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
                                  ),
                                ))),
                      Padding(
                        padding: EdgeInsets.all(0),
                        //rheight: 117,
                        child: Stack(
                          children: [
                            Container(
                              padding:
                                  EdgeInsets.only(left: 22, right: 10, top: 30, bottom: 5),
                                  height: 115,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                color: Colors.white,
                                boxShadow: [
                                      BoxShadow(
                                        
                                        color: Colors.black54,
                                        blurRadius: 5, //độ mềm
                                        spreadRadius: 0, //mở rộng
                                        offset: Offset(
                                          0.0, //Di chuyển sang phải 5 theo chiều ngang
                                          5.0, //Di chuyển xuống dưới cùng 5 Theo chiều dọc
                                          )
                                      )
                                    ]
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Bệnh nhân',
                                        style: TextStyle(
                                            color: Color(0xFF535858),
                                            fontSize: 12),
                                      ),
                                      SizedBox(width: 35),
                                      Text(
                                        '${display[index].name}',
                                        style: TextStyle(
                                            color: Color(0xFF444444),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Phòng khám',
                                        style: TextStyle(
                                            color: Color(0xFF535858),
                                            fontSize: 12),
                                      ),
                                      SizedBox(width: 24),
                                      Text(
                                        '${display[index].phong}',
                                        style: TextStyle(
                                            color: Color(0xFF444444),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Lý do khám',
                                        style: TextStyle(
                                            color: Color(0xFF535858),
                                            fontSize: 12),
                                      ),
                                      SizedBox(width: 30),
                                      Text(
                                        '${display[index].ly_do}',
                                        style: TextStyle(
                                            color: Color(0xFF444444),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Thời gian',
                                        style: TextStyle(
                                            color: Color(0xFF535858),
                                            fontSize: 12),
                                      ),
                                      SizedBox(width: 42),
                                      Text(
                                        '${display[index].time}',
                                        style: TextStyle(
                                            color: Color(0xFF444444),
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            //SizedBox(height: 10),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              height: 23,
                              decoration: BoxDecoration(
                                //borderRadius: BorderRadius.all(Radius.circular(10)),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10)),
                                color: Color(0xFF6f9bd4),
                              ),
                              child: Row(
                                children: [
                                  //Padding(padding: EdgeInsets.symmetric(vertical: 10)),
                                  Text(
                                    'Lịch hẹn khám',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  Spacer(),
                                  Text(
                                    '${display[index].khoang}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
                // return Padding(
                //   padding: const EdgeInsets.symmetric(horizontal: 5),
                //   child: Column(
                //     children: [
                //       if(newDate.isNotEmpty)
                //         Center(child: Container(
                //             // decoration: BoxDecoration(
                //             //     color: Color(0xffE3D4EE),
                //             //     borderRadius: BorderRadius.circular(20)
                //             // ),
                //             child: Padding(
                //               padding: const EdgeInsets.all(8.0),
                //               child: Text(newDate),
                //             ))),
                //       Padding(
                //         padding: const EdgeInsets.symmetric(vertical: 4),
                //         child: CustomPaint(
                //           // painter: MessageBubble(
                //           //     color: main_hen[index].isMe?  const Color(0xffE3D4EE) :  const Color(0xffDAF0F3),
                //           //     alignment: main_hen[index].isMe ? Alignment.topRight : Alignment.topLeft,
                //           //     tail: true
                //           // ),
                //           child: Container(
                //             constraints: BoxConstraints(
                //               maxWidth: MediaQuery.of(context).size.width * .7,
                //             ),

                //             child: Stack(
                //               children: [
                //                 Container(

                //                   child: Text(
                //                     main_hen[index].name  ,
                //                     textAlign: TextAlign.left,
                //                     style: Theme.of(context).textTheme.headline5!.copyWith(
                //                         fontSize: 15 ,

                //                     ),
                //                   ),
                //                 ),
                //                 Positioned(
                //                     bottom: 0,
                //                     right: 0,
                //                     child: Text(messageTime(main_hen[index].day.toString()).toString() ,textAlign: TextAlign.left, style: TextStyle(fontSize: 10),))
                //               ],
                //             ),
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
                // );
              }),
      //),
      //),
      //     ],
      //   ),
      // ),
      // );
    );
  }
}
