import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:product_layout_app/group_listview_henkham.dart';
import 'package:product_layout_app/group_listview_tatca.dart';
import 'package:product_layout_app/group_listview_tuvanonline.dart';


class notifications extends StatelessWidget {
  
  notifications({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.chevron_left),
          ),

          centerTitle: true, //newkt
          title: Text(
            'Thông báo',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: Color(0xFF6f9bd4),
        ),
        body: Container(
          
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/image 1.png'),
              fit: BoxFit.cover,              
            ),
            color: Colors.white.withOpacity(0.8),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Container(
                    width: 280,
                    height: 40,
                    decoration: BoxDecoration(
                        //color: Colors.grey[300],
                        color: Colors.white.withOpacity(0.5), //mờ trong suốt
                        borderRadius: BorderRadius.circular(22.0)),
                    child: TabBar(
                      indicator: BoxDecoration(
                        color: Color(0xFF6f9bd4),
                        borderRadius: BorderRadius.circular(22.0),
                      ),
                      labelColor: Colors.white,
                      labelStyle:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                      unselectedLabelColor: Colors.black,
                      tabs: const [
                        Tab(
                          text: 'Tất cả',
                        ),
                        Tab(
                          text: 'Hẹn khám',
                        ),
                        Tab(
                          text: 'Tư vấn',
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  
                  Expanded(
                      child: TabBarView(
                    children: [
                      
                      allDate(),

                      // Center(child: Text("Hẹn khám"),),
                      groupDate(),                                          

                      // Center(child: Text('Tư vấn online'),),
                      onlineDate(),
                      
                    ],
                  ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
